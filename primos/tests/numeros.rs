use primos::numeros::primos;

#[test]
fn test_primos(){
    let base = vec![
        1, 2, 3, 5, 7, 11, 13, 17,
        19, 23, 29, 31, 37, 41,
        43, 47, 53, 59, 61, 67,
        71, 73, 79, 83, 89, 97];

    let lista = primos(100).unwrap();

    assert_eq!(base, lista);
}
