use std::error::Error;


/// Crea una lista de números primos hasta un valor límite N
/// N debe ser un entero positivo.
pub fn primos(n:u64) -> Result<Vec<u64>,Box<dyn Error>>{
    let mut set_primos = vec![1];
    for i in 2..n {
        let mut bandera = true;
        for j in set_primos.iter() {
            if i % *j == 0 && *j > 1 {
                bandera = false;
                break
            }
        }
        if bandera {
            set_primos.push(i);
        }
    }
    Ok(set_primos)
}
