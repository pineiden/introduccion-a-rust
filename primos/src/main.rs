use std::env;
use primos::numeros::primos;

fn main() {
    let args: Vec<String> = env::args().collect();
    let number = args[1].parse::<u64>().unwrap();
    println!("Buscando los primos hasta: {}", number);
    let set_primos = primos(number);
    //println!("{:?}", set_primos);

    match set_primos {
        Ok(list) =>  {
            for nro in list.iter() {
                println!("{}",nro);
            }
        },
        Err(_) => println!("Error")
    }
}
